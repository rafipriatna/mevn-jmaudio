// Component
const IndexView = () => import("@/views/admin/merek-barang/MerekBarang.vue");

// Routes
export default [
  {
    path: "/merek-barang",
    name: "MerekBarang",
    component: IndexView,
    meta: {
      title: `Merek Barang — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
];
