import axios from "../../axios";

const state = {};
const getters = {};
const actions = {
  async update_profile(context, data) {
    let res = await axios.patch(`/my/`, data);
    return res.data;
  },
  async ganti_password(context, data) {
    let res = await axios.patch(`/my/gantipassword`, data);
    return res.data;
  },
};
const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
