const merekBarangModel = require("../../models/merek_barang.model");

exports.create = (req, res) => {
  let newMerekBarang = new merekBarangModel(req.body);
  newMerekBarang.save(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: `${data.nama_merek_barang} berhasil dibuat.`,
      data: data,
    });
  });
};

exports.getAll = (req, res) => {
  merekBarangModel.find(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: "Data merek barang.",
      data: data,
    });
  });
};

exports.getById = (req, res) => {
  merekBarangModel.findById(req.params.id, function (err, data) {
    if (data == null)
      return res.status(404).json({
        message: "Data tidak ditemukan.",
      });

    res.status(200).json({
      message: "Data merek barang.",
      data: data,
    });
  });
};

exports.update = (req, res) => {
  merekBarangModel.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      upsert: true,
      new: true,
    },
    function (err, data) {
      if (err) return res.status(500).json(err);
      res.status(200).json({
        message: `${data.nama_merek_barang} berhasil diperbarui!`,
        data: data,
      });
    }
  );
};

exports.delete = (req, res) => {
  merekBarangModel.findByIdAndDelete(req.params.id, function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: `${data.nama_merek_barang} berhasil dihapus!`,
    });
  });
};
