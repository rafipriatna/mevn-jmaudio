// Component
const IndexView = () => import("@/views/admin/rekening/Rekening.vue");

// Routes
export default [
  {
    path: "/rekening",
    name: "Rekening",
    component: IndexView,
    meta: {
      title: `Rekening — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
];
