const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require("path");
const cors = require("cors");
const passport = require("passport");
const app = express();

// CORS
let allowList = ["http://localhost:8081", "http://localhost:3000"];

let corsOption = {
  credentials: true,
  origin: (origin, callback) => {
    if (allowList.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error("Not allowed."));
    }
  },
};

// Middleware
app.use(cors(corsOption));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Public dir as static dir
app.use(express.static(path.join(__dirname, "public")));

// Passport configuration
app.use(passport.initialize());
require("./app/config/passport.config")(passport);

// Database config
const db = require("./app/config/mongo.config");
mongoose.set("useCreateIndex", true);
mongoose
  .connect(db.mongoURI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log(`Database is connected to ${db.mongoURI}`);
  })
  .catch((err) => {
    console.log(`Unable to connect with the database ${err}`);
  });

// Routes
const auth = require("./app/routes/auth.route");
const my = require("./app/routes/my.route");

// Admin Routes
const users = require("./app/routes/admin/user.route");
const barang = require("./app/routes/admin/barang.route");
const jenisBarang = require("./app/routes/admin/jenis_barang.route");
const merekBarang = require("./app/routes/admin/merek_barang.route");
const paket = require("./app/routes/admin/paket.route");

// Pegawai Routes
const pelanggan = require("./app/routes/pelanggan.route");
const transaksi = require("./app/routes/transaksi.route");
const rekening = require("./app/routes/admin/rekening.route");
const laporan = require("./app/routes/laporan.route");
const dashboard = require("./app/routes/dashboard.route");
const galeri = require("./app/routes/galeri.route");

// Public Routes
const publicPaket = require("./app/routes/umum/paket.route");
const publicAuth = require("./app/routes/umum/auth.route");
const publicMe = require("./app/routes/umum/me.route");
const publicTransaksi = require("./app/routes/umum/transaksi.route");
const publicRekening = require("./app/routes/umum/rekening.route");
const publicGaleri = require("./app/routes/umum/galeri.route");

app.get("/", (req, res) => {
  res.json({
    message: "Halo Dunia!",
  });
});

app.use("/api/auth", auth);
app.use("/api/my", my);

app.use("/api/admin/users", users);
app.use("/api/admin/barang", barang);
app.use("/api/admin/jenis_barang", jenisBarang);
app.use("/api/admin/merek_barang", merekBarang);
app.use("/api/admin/paket", paket);
app.use("/api/admin/rekening", rekening);

app.use("/api/pegawai/pelanggan", pelanggan);
app.use("/api/pegawai/transaksi", transaksi);
app.use("/api/pegawai/laporan", laporan);
app.use("/api/pegawai/dashboard", dashboard);
app.use("/api/pegawai/galeri", galeri);

app.use("/api/public/paket", publicPaket);
app.use("/api/public/auth", publicAuth);
app.use("/api/public/me", publicMe);
app.use("/api/public/transaksi", publicTransaksi);
app.use("/api/public/rekening", publicRekening);
app.use("/api/public/galeri", publicGaleri);

// Start server
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server started on port http://localhost:${PORT}`);
});
