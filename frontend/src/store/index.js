import Vue from "vue";
import Vuex from "vuex";

import Auth from "./modules/Auth";
import User from "./modules/User";
import Barang from "./modules/Barang";
import JenisBarang from "./modules/JenisBarang";
import MerekBarang from "./modules/MerekBarang";
import Paket from "./modules/Paket";
import Rekening from "./modules/Rekening";
import Pelanggan from "./modules/Pelanggan";
import Transaksi from "./modules/Transaksi";
import Laporan from "./modules/Laporan";
import Dashboard from "./modules/Dashboard";
import Galeri from "./modules/Galeri";
import My from "./modules/My";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  getters: {},
  actions: {},
  modules: {
    Auth,
    User,
    Barang,
    JenisBarang,
    MerekBarang,
    Paket,
    Rekening,
    Pelanggan,
    Transaksi,
    Laporan,
    Dashboard,
    Galeri,
    My
  },
});
