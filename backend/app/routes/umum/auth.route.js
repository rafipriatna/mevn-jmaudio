const express = require("express");
const router = express.Router();
const model = require("../../controllers/pelanggan.controller");

router.post("/register", model.create);

router.post("/", model.login);

module.exports = router;
