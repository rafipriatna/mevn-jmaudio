import axios from 'axios'

export const actions = {
  async get_all_paket() {
    let res = await axios.get(`${this.$axios.defaults.baseURL}/paket`)
    return res.data.data
  },
  async get_paket(context, id) {
    let res = await axios.get(
      `${this.$axios.defaults.baseURL}/paket/${id}`
    )
    return res.data.data
  },
}

