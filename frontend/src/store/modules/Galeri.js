import axios from "../../axios";

const state = {};
const getters = {};
const actions = {
    async get_all_galeri() {
        let res = await axios.get("/pegawai/galeri");
        return res.data.data;
    },
    async create_new_galeri(context, data) {
        let res = await axios.post("/pegawai/galeri", data);
        return res.data.message;
    },
    async get_galeri_by_id(context, id) {
        let res = await axios.get(`/pegawai/galeri/${id}`);
        return res.data.data;
    },
    async update_galeri(context, data) {
        const id = data.id ? data.id : data.get("id");
        let res = await axios.patch(`/pegawai/galeri/data/${id}`, data);
        return res.data.message;
    },
    async tambah_foto(context, data) {
        const id = data.get("id");
        let res = await axios.patch(`/pegawai/galeri/foto/${id}`, data);
        return res.data.message;
    },
    async hapus_foto(context, data) {
        const id = data.id;
        let res = await axios.patch(`/pegawai/galeri/hapus/${id}`, data);
        return res.data.message;
    },
    async hapus_galeri(context, id) {
        let res = await axios.delete(`/pegawai/galeri/${id}`);
        return res.data.message;
    },
};
const mutations = {};

export default {
    state,
    actions,
    mutations,
    getters,
};
