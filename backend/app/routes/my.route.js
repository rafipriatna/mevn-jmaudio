const express = require("express");
const passport = require("passport");
const user = require("../controllers/admin/user.controller");

const router = express.Router();

router.get(
  "/",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  (req, res) => {
    return res.status(200).json({
      message: "Profile",
      user: req.user,
    });
  }
);

router.patch(
  "/",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  user.update
);

router.patch(
  "/gantipassword",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  user.gantiPassword
);

module.exports = router;
