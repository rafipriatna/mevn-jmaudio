const express = require("express");
const passport = require("passport");

const model = require("../../controllers/pelanggan.controller");

const router = express.Router();

/*
 * Routes to handle current user data.
 */

router.get(
  "/",
  passport.authenticate("pelangganAuth", {
    session: false,
  }),
  (req, res) => {
    return res.status(200).json({
      message: "Profile",
      user: req.user,
    });
  }
);

router.patch(
  "/",
  passport.authenticate("pelangganAuth", {
    session: false,
  }),
  model.update
);

router.patch(
  "/gantipassword",
  passport.authenticate("pelangganAuth", {
    session: false,
  }),
  model.gantiPassword
);

module.exports = router;
