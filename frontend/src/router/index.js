import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store/index";
import Home from "../views/Home.vue";

// Modules
import moduleUsers from "./modules/admin/Users";
import moduleBarang from "./modules/admin/Barang";
import moduleJenisBarang from "./modules/admin/JenisBarang";
import moduleMerekBarang from "./modules/admin/MerekBarang";
import modulePaket from "./modules/admin/Paket";
import moduleRekening from "./modules/admin/Rekening";
import modulePelanggan from "./modules/Pelanggan";
import moduleTransaksi from "./modules/Transaksi";
import moduleLaporan from "./modules/Laporan";
import moduleGaleri from "./modules/Galeri";

// Routes
Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: () => import("../views/auth/Login.vue"),
    meta: {
      title: `Login — ${process.env.VUE_APP_SITE_TITLE}`,
    },
  },
  {
    path: "/",
    name: "Home",
    component: Home,
    meta: {
      title: `Home — ${process.env.VUE_APP_SITE_TITLE}`,
    },
  },
  {
    path: "/my",
    name: "My",
    component: () => import("../views/my/Profile.vue"),
    meta: {
      title: `My Profile — ${process.env.VUE_APP_SITE_TITLE}`,
    },
  },
  {
    path: "/404",
    alias: "*",
    name: "PageNotFound",
    meta: {
      title: `404 — ${process.env.VUE_APP_SITE_TITLE}`,
    },
  },
  ...moduleUsers,
  ...moduleBarang,
  ...moduleJenisBarang,
  ...moduleMerekBarang,
  ...modulePaket,
  ...moduleRekening,
  ...modulePelanggan,
  ...moduleTransaksi,
  ...moduleLaporan,
  ...moduleGaleri,
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const isUserAdmin = store.getters.isUserAdmin;
  const isAdmin = to.matched.some((record) => record.meta.isAdmin);
  const isLoggedIn = store.getters.isLoggedIn;

  if (to.name !== "Login" && !isLoggedIn) next({ name: "Login" });
  if (to.name === "Login" && isLoggedIn) next({ name: "Home" });
  if (isAdmin && !isUserAdmin) {
    next({ name: "PageNotFound" });
  } else {
    next();
  }
});

export default router;
