import axios from "../../axios";

const state = {};
const getters = {};
const actions = {
  async get_all_rekening() {
    let res = await axios.get("/admin/rekening");
    return res.data.data;
  },
  async create_new_rekening(context, data) {
    let res = await axios.post("/admin/rekening", data);
    return res.data.message;
  },
  async get_rekening(context, id) {
    let res = await axios.get(`/admin/rekening/${id}`);
    return res.data.data;
  },
  async update_rekening(context, data) {
    let id = data.id;
    let res = await axios.patch(`/admin/rekening/${id}`, data);
    return res.data.message;
  },
  async delete_rekening(context, id) {
    let res = await axios.delete(`/admin/rekening/${id}`);
    return res.data.message;
  },
};
const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
