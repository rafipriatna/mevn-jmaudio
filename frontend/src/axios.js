import axios from "axios";
import SecureLS from "secure-ls";
import store from "./store/index";

const ls = new SecureLS({
  encodingType: "aes",
  encryptionSecret: `${process.env.VUE_APP_LS_ENCRYPTION_KEY}`,
});

let headers = {
  "cache-control": "no-cache",
};

const accessToken = ls.get("token");

if (accessToken && accessToken !== "") {
  headers.Authorization = accessToken;
}

const instance = axios.create({
  baseURL: `${process.env.VUE_APP_API_URL}/api`,
  headers: headers,
});

const currentPath = window.location.pathname;

instance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response !== undefined) {
      if (error.response.status === 401) {
        if (currentPath != "/login") {
          store.dispatch("logout");
        }
      }
    }
    return Promise.reject(error);
  }
);

export default instance;
