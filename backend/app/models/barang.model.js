const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BarangSchema = new Schema(
  {
    foto_barang: {
      type: String,
      required: true,
    },
    jenis_barang_id: {
      type: Schema.Types.ObjectId,
      ref: "jenis_barang",
      required: true,
    },
    merek_barang_id: {
      type: Schema.Types.ObjectId,
      ref: "merek_barang",
      required: true,
    },
    nama_barang: {
      type: String,
      required: true,
    },
    no_seri_barang: {
      type: String,
      required: true,
      unique: true,
    },
    stok_barang: {
      type: Number,
      required: true,
    },
    harga_beli_barang: {
      type: Number,
      required: true,
    },
    tanggal_beli_barang: {
      type: Date,
      required: true,
    },
  },
  { collection: "barang" }
);

module.exports = Barang = mongoose.model("barang", BarangSchema);
