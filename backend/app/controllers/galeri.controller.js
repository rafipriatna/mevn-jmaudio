const multer = require("multer");
const fs = require("fs");
const model = require("../models/galeri.model");

const path = "public/galeri/";
const fileFilter = (req, file, callback) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    callback(null, true);
  } else {
    callback(null, false);
  }
};

const upload = multer({
  dest: path,
  limits: {
    fieldSize: 1024 * 1024 * 3, // 3MB
  },
  fileFilter: fileFilter,
}).array("file");

const uploadSatu = multer({
  dest: path,
  limits: {
    fieldSize: 1024 * 1024 * 3, // 3MB
  },
  fileFilter: fileFilter,
}).single("file");

exports.getAll = (req, res) => {
  model.find(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: "Data galeri.",
      data: data,
    });
  });
};

exports.create = (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      res.status(500).json({
        message: "File upload error. Format file harus JPG atau PNG.",
      });
    } else {
      let foto_galeri = [];
      // Looping foto
      req.files.map((file) => {
        foto_galeri.push({
          file: file.filename,
        });
      });
      // Simpan ke DB
      req.body.foto_galeri = foto_galeri;
      const newGaleri = new model(req.body);

      newGaleri.save(function (err, data) {
        if (err) {
          req.files.map((file) => {
            fs.unlinkSync(path + file.filename);
          });
          res.status(500).json(err);
        } else {
          res.status(200).json({
            message: `${data.judul_galeri} berhasil dibuat.`,
            data: data,
          });
        }
      });
    }
  });
};

exports.getById = (req, res) => {
  model.findById(req.params.id, function (err, data) {
    if (err) return res.status(500).json(err);
    if (data == null)
      return res.status(404).json({
        message: "Galeri tidak ditemukan.",
      });
    res.status(200).json({
      message: "Data galeri.",
      data: data,
    });
  });
};

exports.update = (req, res) => {
  model.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      upsert: true,
      new: true,
    },
    function (err, data) {
      if (err) return res.status(500).json(err);
      res.status(200).json({
        message: `${data.judul_galeri} berhasil diperbarui!`,
        data: data,
      });
    }
  );
};

exports.tambahFoto = (req, res) => {
  uploadSatu(req, res, (err) => {
    if (err) {
      res.status(500).json({
        message: "File upload error. Format file harus JPG atau PNG.",
      });
    } else {
      // Simpan ke DB
      model.findById(req.params.id, function (err, data) {
        if (err) return res.status(500).json(err);

        let foto_galeri = [];
        data.foto_galeri.map((file) => foto_galeri.push(file));

        foto_galeri.push({ file: req.file.filename });

        model.updateOne(
          { _id: req.params.id },
          { foto_galeri: foto_galeri },
          function (err) {
            if (err) return res.status(500).json(err);

            res.status(200).json({
              message: `Berhasil menambahkan foto!`,
              data: data,
            });
          }
        );
      });
    }
  });
};

exports.hapusFoto = (req, res) => {
  model.findById(req.params.id, function (err, data) {
    if (err) return res.status(500).json(err);
    if (fs.existsSync(path + req.body.file)) {
      fs.unlinkSync(path + req.body.file);
    }

    let foto_galeri = [];
    data.foto_galeri
      .filter((file) => file.file != req.body.file)
      .map((file) => foto_galeri.push(file));

    model.updateOne(
      { _id: req.params.id },
      { foto_galeri: foto_galeri },
      function (err) {
        if (err) return res.status(500).json(err);
        res.status(200).json({
          message: `Berhasil menghapus foto!`,
          data: data,
        });
      }
    );
  });
};

exports.hapusGaleri = (req, res) => {
  model.findByIdAndDelete(req.params.id, function (err, data) {
    if (err) return res.status(500).json(err);

    // Hapus foto
    data.foto.map((file) => {
      try {
        if (fs.existsSync(path + file.file)) {
          fs.unlinkSync(path + file.file);
        }
      } catch (err) {
        res.status(500).json(err);
      }
    });

    res.status(200).json({
      message: `${data.judul_galeri} berhasil dihapus!`,
    });
  });
};
