// Component
const indexView = () =>
  import(/* webpackChunkName: "group-users" */ "@/views/admin/pengguna/Pengguna.vue");
const createView = () =>
  import(
    /* webpackChunkName: "group-users" */ "@/views/admin/pengguna/Tambah.vue"
  );
const editView = () =>
  import(/* webpackChunkName: "group-users" */ "@/views/admin/pengguna/Ubah.vue");

// Routes
export default [
  {
    path: "/pengguna",
    name: "Pengguna",
    component: indexView,
    meta: {
      title: `Data Pengguna — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
  {
    path: "/pengguna/tambah",
    name: "TambahPengguna",
    component: createView,
    meta: {
      title: `Tambah Pengguna — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
  {
    path: "/pengguna/:id",
    name: "UbahPengguna",
    component: editView,
    meta: {
      title: `Ubah Pengguna — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
];
