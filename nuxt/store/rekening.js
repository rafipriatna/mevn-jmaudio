import axios from 'axios'

export const actions = {
  async get_all_rekening() {
    let res = await axios.get(`${this.$axios.defaults.baseURL}/rekening`)
    return res.data.data
  },
}
