const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  foto_user: {
    type: String,
  },
  email_user: {
    type: String,
    required: true,
    unique: true,
  },
  password_user: {
    type: String,
    required: true,
    min: 8,
  },
  nama_user: {
    type: String,
    required: true,
  },
  level_user: {
    type: String,
    enum: ["Admin", "Pegawai"],
  },
  last_access_user: {
    type: Date,
    default: null,
  },
});

userSchema.pre("save", function (next) {
  //Mongoose middleware, performs password hashing before saving data into database
  let user = this;
  bcrypt.genSalt(10, (error, salt) => {
    bcrypt.hash(user.password_user, salt, (error, hash) => {
      user.password_user = hash;
      next();
    });
  });
});

userSchema.pre("findOneAndUpdate", function (next) {
  let user = this._update;
  let isModified = this.getUpdate().password_user;
  if (isModified) {
    //Check if the password is modifed, if so then only perform hasing
    bcrypt.genSalt(10, (error, salt) => {
      bcrypt.hash(user.password_user, salt, (error, hash) => {
        user.password_user = hash;
        next();
      });
    });
  } else {
    next();
  }
});

const userModel = mongoose.model("users", userSchema);

module.exports = userModel;
