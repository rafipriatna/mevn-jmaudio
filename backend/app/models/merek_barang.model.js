const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const MerekBarangSchema = new Schema({
  nama_merek_barang: {
    type: String,
    required: true,
  },
},{ collection: "merek_barang" });

module.exports = Barang = mongoose.model("merek_barang", MerekBarangSchema);
