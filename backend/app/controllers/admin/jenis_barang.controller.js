const jenisBarangModel = require("../../models/jenis_barang.model");

exports.create = (req, res) => {
  let newJenisBarang = new jenisBarangModel(req.body);
  newJenisBarang.save(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: `${data.nama_jenis_barang} berhasil dibuat.`,
      data: data,
    });
  });
};

exports.getAll = (req, res) => {
  jenisBarangModel.find(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: "Data jenis barang.",
      data: data,
    });
  });
};

exports.getById = (req, res) => {
  jenisBarangModel.findById(req.params.id, function (err, data) {
    if (data == null)
      return res.status(404).json({
        message: "Data tidak ditemukan.",
      });

    res.status(200).json({
      message: "Data jenis barang.",
      data: data,
    });
  });
};

exports.update = (req, res) => {
  jenisBarangModel.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      upsert: true,
      new: true,
    },
    function (err, data) {
      if (err) return res.status(500).json(err);
      res.status(200).json({
        message: `${data.nama_jenis_barang} berhasil diperbarui!`,
        data: data,
      });
    }
  );
};

exports.delete = (req, res) => {
  jenisBarangModel.findByIdAndDelete(req.params.id, function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: `${data.nama_jenis_barang} berhasil dihapus!`,
    });
  });
};
