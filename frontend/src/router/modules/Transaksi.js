// Component
const indexView = () =>
  import(
    /* webpackChunkName: "group-transaksi" */ "@/views/transaksi/Transaksi.vue"
  );
const lihatView = () =>
  import(
    /* webpackChunkName: "group-transaksi" */ "@/views/transaksi/Lihat.vue"
  );

// Routes
export default [
  {
    path: "/transaksi",
    name: "Transaksi",
    component: indexView,
    meta: {
      title: `Data Transaksi — ${process.env.VUE_APP_SITE_TITLE}`,
    },
  },
  {
    path: "/transaksi/:id",
    name: "LihatTransaksi",
    component: lihatView,
    meta: {
      title: `Info Transaksi — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
];
