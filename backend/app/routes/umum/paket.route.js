const express = require("express");
const passport = require("passport");

const router = express.Router();

const model = require("../../controllers/admin/paket.controller");

router.get(
  "/",
  model.getAll
);

router.get(
  "/:id",
  model.getById
);

module.exports = router;
