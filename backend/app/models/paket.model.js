const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PaketSchema = new Schema(
  {
    nama_paket: {
      type: String,
      required: true,
    },
    watt_paket: {
      type: Number,
      required: true,
    },
    keterangan_paket: {
      type: String,
      required: true,
    },
    harga_paket: {
      type: Number,
      required: true,
    },
    barang_paket: [
      {
        data_barang: {
          type: Schema.ObjectId,
          ref: "barang",
          required: true,
        },
        jumlah_barang: {
          type: Number,
          require: true,
        },
      },
    ],
    foto_paket: {
      type: String,
      required: true,
    },
  },
  { collection: "paket" }
);

module.exports = mongoose.model("paket", PaketSchema);
