import axios from 'axios'

export const getters = {
  isAuthenticated(state) {
    return state.auth.loggedIn
  },

  loggedInUser(state) {
    return state.auth.user
  },
}

export const actions = {
  async register(context, data) {
    let res = await axios.post(
      `${this.$axios.defaults.baseURL}/auth/register`,
      data
    )
    return res.data.message
  },

  async updateUser(context, data) {
    if (process.browser) {
      const id = data.id
      const token = localStorage.getItem('auth._token.local')
      let res = await axios.patch(
        `${this.$axios.defaults.baseURL}/me/${id}`,
        data,
        {
          headers: {
            Authorization: token,
          },
        }
      )
      return res.data.message
    }
  },

  async updatePassword(context, data) {
    if (process.browser) {
      const id = data.id
      const token = localStorage.getItem('auth._token.local')
      let res = await axios.patch(
        `${this.$axios.defaults.baseURL}/me/${id}`,
        data,
        {
          headers: {
            Authorization: token,
          },
        }
      )
      return res.data.message
    }
  },
}
