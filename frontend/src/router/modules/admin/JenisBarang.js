// Component
const IndexView = () => import("@/views/admin/jenis-barang/JenisBarang.vue");

// Routes
export default [
  {
    path: "/jenis-barang",
    name: "JenisBarang",
    component: IndexView,
    meta: {
      title: `Jenis Barang — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
];
