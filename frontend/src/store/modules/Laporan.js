import axios from "../../axios";

const state = {};
const getters = {};
const actions = {
  async get_laporan() {
    let res = await axios.get("/pegawai/laporan");
    return res.data.data;
  },
  async get_laporan_date(context, data) {
    let res = await axios.post('/pegawai/laporan/', data);
    console.log(res)
    return res.data.data;
  },
};
const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
