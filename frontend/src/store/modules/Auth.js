import router from "../../router";
import VueJwtDecode from "vue-jwt-decode";
import axios from "../../axios";
import SecureLS from "secure-ls";
const ls = new SecureLS({
  encodingType: "aes",
  encryptionSecret: `${process.env.VUE_APP_LS_ENCRYPTION_KEY}`,
});

const state = {
  token: ls.get("token") || "",
  status: "",
  lastAccess: ls.get("lastAccess") || "",
  userLevel: ls.get("userLevel") || "",
};

const getters = {
  isLoggedIn: (state) => !!state.token,
  authState: (state) => state.status,
  lastAccess: (state) => state.lastAccess,
  userLevel: (state) => state.userLevel,
  isUserAdmin: (state) => !!(state.userLevel == "Admin"),
};

const actions = {
  async login({ commit }, user) {
    commit("auth_request");
    let res = await axios.post("/auth", user);
    if (res.data.token) {
      const token = res.data.token;

      const tokenSplit = token.split("Bearer ").join("");
      const lastAccessDate = VueJwtDecode.decode(tokenSplit).last_access_user;
      const userLevel = await VueJwtDecode.decode(tokenSplit).level_user;

      ls.set("token", token);
      ls.set("lastAccess", lastAccessDate);
      ls.set("userLevel", userLevel);

      axios.defaults.headers.common["Authorization"] = token;

      commit("auth_success", token);
    }
    return res;
  },
  async logout({ commit }) {
    commit("auth_logout");
    await ls.removeAll();
    delete axios.defaults.headers.common["Authorization"];
    router.push("/login");
    return;
  },
};

const mutations = {
  auth_request(state) {
    state.status = "waiting";
  },
  auth_success(state, token) {
    (state.token = token), (state.status = "success");
  },
  get_last_access(state, lastAccess) {
    state.lastAccess = lastAccess;
  },
  auth_logout(state) {
    state.status = "";
    state.token = "";
    state.lastAccess = "";
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
