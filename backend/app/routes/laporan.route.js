const express = require("express");
const passport = require("passport");

const router = express.Router();

const controller = require("../controllers/laporan.controller");

router.get(
  "/",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  controller.bulanIni
);

router.post(
  "/",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  controller.pilihTanggal
);

module.exports = router;
