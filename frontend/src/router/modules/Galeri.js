// Component
const indexView = () =>
    import(/* webpackChunkName: "group-galeri" */  "@/views/galeri/Galeri.vue"
    );
const createView = () =>
    import(/* webpackChunkName: "group-galeri" */ "@/views/galeri/Tambah.vue");
const editView = () =>
    import(
    /* webpackChunkName: "group-galeri" */ "@/views/galeri/Ubah.vue"
    );

// Routes
export default [
    {
        path: "/galeri",
        name: "Galeri",
        component: indexView,
        meta: {
            title: `Galeri — ${process.env.VUE_APP_SITE_TITLE}`,
        },
    },
    {
        path: "/galeri/tambah",
        name: "TambahGaleri",
        component: createView,
        meta: {
            title: `Tambah Galeri — ${process.env.VUE_APP_SITE_TITLE}`,
        },
    },
    {
        path: "/galeri/:id",
        name: "UbahGaleri",
        component: editView,
        meta: {
          title: `Ubah Galeri — ${process.env.VUE_APP_SITE_TITLE}`,
          isAdmin: true,
        },
      },
];
