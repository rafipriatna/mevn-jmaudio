import axios from "../../axios";

const state = {
  status: "",
  user: {},
  error: null,
};

const getters = {
  status: (state) => state.status,
  user: (state) => state.user,
  error: (state) => state.error,
};

const actions = {
  async getUserData({ commit }) {
    commit("user_request");
    let res = await axios.get("/my");
    commit("user_data", res.data.user);
    return res;
  },
  async get_all_users() {
    let res = await axios.get("/admin/users");
    return res.data.data;
  },
  async create_new_user(context, user) {
    let res = await axios.post("/admin/users", user);
    return res.data.data;
  },
  async get_user_by_id(context, id) {
    let res = await axios.get(`/admin/users/${id}`);
    return res.data.data;
  },
  async update_user(context, data) {
    let id = data.id;
    let user = {
      nama_user: data.nama_user,
      email_user: data.email_user,
      level_user: data.level_user,
    };
    let res = await axios.patch(`/admin/users/${id}`, user);
    return res.data.data;
  },
  async update_user_image(context, data) {
    let id = data.get("id");
    let res = await axios.patch(`/admin/users/${id}`, data);
    return res.data.data;
  },
  async change_password(context, data) {
    let id = data.id;
    let user = { password_user: data.password_user };
    let res = await axios.patch(`/admin/users/${id}`, user);
    return res.data.data;
  },
  async delete_user(context, id) {
    let res = await axios.delete(`/admin/users/${id}`);
    return res.data;
  },
};

const mutations = {
  user_request(state) {
    state.status = "loading";
  },
  user_data(state, user) {
    state.status = "success";
    state.user = user;
  },
};

export default {
  state,
  actions,
  mutations,
  getters,
};
