// Component
const indexView = () =>
  import(
    "@/views/laporan/Laporan.vue"
  );

// Routes
export default [
  {
    path: "/laporan",
    name: "Laporan",
    component: indexView,
    meta: {
      title: `Laporan — ${process.env.VUE_APP_SITE_TITLE}`,
    },
  },
];
