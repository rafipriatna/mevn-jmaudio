const express = require("express");
const passport = require("passport");

const router = express.Router();

const controller = require("../../controllers/rekening.controller");

router.post(
  "/",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  controller.create
);

router.get(
  "/",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  controller.getAll
);

router.get(
  "/:id",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  controller.getById
);

router.patch(
  "/:id",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  controller.update
);

router.delete(
  "/:id",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  controller.delete
);

module.exports = router;
