export const getters = {
  isAuthenticated(state) {
    return state.auth.loggedIn
  },

  loggedInUser(state) {
    return state.auth.user
  },

  berhasilDaftar (state) {
    return state.daftar
  }
}

export const actions = {
  async register({ commit }, data) {
    await this.$axios.$post(
      `${this.$axios.defaults.baseURL}/auth/register`,
      data
    ).then(res => {
      return res.message
    })
  },

  async updateUser(context, data) {
    if (process.browser) {
      const token = localStorage.getItem('auth._token.local')
      let res = await this.$axios.$patch(
        `${this.$axios.defaults.baseURL}/me/`,
        data,
        {
          headers: {
            Authorization: token,
          },
        }
      )
      return res.message
    }
  },

  async updatePassword(context, data) {
    if (process.browser) {
      const token = localStorage.getItem('auth._token.local')
      let res = await this.$axios.$patch(
        `${this.$axios.defaults.baseURL}/me/gantipassword`,
        data,
        {
          headers: {
            Authorization: token,
          },
        }
      )
      return res.message
    }
  },
}
