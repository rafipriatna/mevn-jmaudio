const multer = require("multer");
const fs = require("fs");
const moment = require("moment");
const model = require("../models/transaksi.model");
const paket = require("../models/paket.model");

// File Upload
const path = "public/bukti_pembayaran/";
const fileFilter = (req, file, callback) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    callback(null, true);
  } else {
    callback(null, false);
  }
};
const upload = multer({
  dest: path,
  limits: {
    fieldSize: 1024 * 1024 * 3, // 3MB
  },
  fileFilter: fileFilter,
}).single("pembayaran_transaksi.bukti_pembayaran");

exports.create = (req, res) => {
  const tanggalSekarang = new Date();

  const no_transaksi = moment(tanggalSekarang).format("DDMMYYYYHHMMSS");

  // Hitung lamanya sewa
  const dariTanggal = moment(req.body.tanggal_transaksi.dari);
  const sampaiTanggal = moment(req.body.tanggal_transaksi.sampai);
  const totalHariSewa =
    moment.duration(sampaiTanggal.diff(dariTanggal)).asDays() + 1;

  // Simpan Transaksi
  paket.findById(req.body.paket_transaksi, function (err, dataPaket) {
    if (dataPaket == null)
      return res.status(404).json({
        message: "Paket tidak ditemukan.",
      });
    if (err) return res.status(500).json(err);

    const totalHarga = totalHariSewa * dataPaket.harga_paket;
    const dp_transaksi = totalHarga * (50 / 100);

    const transaksi = {
      no_transaksi: no_transaksi,
      pelanggan_id: req.body.pelanggan_id,
      paket_transaksi: dataPaket,
      catatan_transaksi: req.body.catatan_transaksi,
      dp_transaksi: dp_transaksi,
      total_harga_transaksi: totalHarga,
      status_transaksi: "Belum Bayar",
      tanggal_transaksi: {
        dari: dariTanggal,
        sampai: sampaiTanggal,
      },
    };

    let newTransaksi = new model(transaksi);
    newTransaksi.save(function (err, dataTransaksi) {
      if (err) return res.status(500).json(err);

      res.status(200).json({
        message: `Transaksi berhasil dibuat.`,
        data: dataTransaksi,
      });
    });
  });
};

exports.getAll = (req, res) => {
  model
    .find(function (err, data) {
      if (data == null)
        return res.status(404).json({
          message: "Data tidak ditemukan.",
        });

      res.status(200).json({
        message: "Transaksi.",
        data: data,
      });
    })
    .populate("pelanggan_id", "nama_pelanggan")
    .populate("pembayaran_transaksi.rekening_tujuan_id");
};

exports.getById = (req, res) => {
  model
    .findById(req.params.id, function (err, data) {
      if (data == null)
        return res.status(404).json({
          message: "Data tidak ditemukan.",
        });

      res.status(200).json({
        message: "Transaksi Anda.",
        data: data,
      });
    })
    .populate("pelanggan_id")
    .populate("pembayaran_transaksi.rekening_tujuan_id");
};

exports.updateStatus = (req, res) => {
  model.findByIdAndUpdate(
    req.params.id,
    {
      status_transaksi: req.body.status_transaksi,
    },
    function (err, data) {
      if (err) return res.status(500).json(err);

      res.status(200).json({
        message: "Berhasil memperbarui status transaksi.",
        data: data,
      });
    }
  );
};

exports.uploadBuktiPembayaran = (req, res) => {
  if (
    req.body.nama_pengirim &&
    req.body.bank_pengirim &&
    req.body.rekening_pengirim &&
    req.body.rekening_tujuan_id
  ) {
    res.status(403).json({
      message:
        "Nama pengirim, Bank pengirim, Rekening tujuan, dan file bukti pembayaran harus diisi.",
    });
  } else {
    upload(req, res, (err) => {
      if (err) {
        res.status(500).json({
          message: "File upload error. Format file harus JPG atau PNG.",
        });
      } else {
        if (req.file) {
          req.body["pembayaran_transaksi.bukti_pembayaran"] = req.file.filename;
        }
        req.body.status_transaksi = "Menunggu Konfirmasi Pembayaran";
        model.findByIdAndUpdate(
          req.params.id,
          req.body,
          {
            runValidators: true,
          },
          function (err, currentTransaction) {
            if (err) {
              return res.status(500).json(err);
            }

            // Delete current photo
            if (req.file) {
              if (
                fs.existsSync(
                  path +
                    currentTransaction.pembayaran_transaksi.bukti_pembayaran
                )
              ) {
                fs.unlinkSync(
                  path +
                    currentTransaction.pembayaran_transaksi.bukti_pembayaran
                );
              }
            }
            res.status(200).json({
              message: `${currentTransaction.no_transaksi} berhasil diperbarui!`,
              data: currentTransaction,
            });
          }
        );
      }
    });
  }
};

exports.batalkanPesanan = (req, res) => {
  model.findByIdAndUpdate(
    req.params.id,
    {
      status_transaksi: "Dibatalkan",
    },
    function (err, data) {
      if (err) return res.status(500).json(err);

      res.status(200).json({
        message: "Transaksi berhasil dibatalkan.",
        data: data,
      });
    }
  );
};

exports.transaksiBulanIni = (req, res) => {
  const bulanIni = moment().startOf("month");
  model
    .find(
      {
        createdAt: {
          $gte: bulanIni.toDate(),
          $lte: moment(bulanIni).endOf("month").toDate(),
        },
      },
      function (err, data) {
        if (err) return res.status(500).json(err);

        res.status(200).json({
          message: "Transaksi bulan ini.",
          data: data,
        });
      }
    )
    .populate("pelanggan_id")
    .populate("pembayaran_transaksi.rekening_tujuan_id");
};
