export const actions = {
  async getAllPaket() {
    let res = await this.$axios.$get(`${this.$axios.defaults.baseURL}/paket`)
    return res.data
  },
  async getPaket(context, id) {
    let res = await this.$axios.$get(
      `${this.$axios.defaults.baseURL}/paket/${id}`
    )
    return res.data
  },
  async beliPaket(context, data) {
    let res = await this.$axios.$post(`${this.$axios.defaults.baseURL}/transaksi/`, data)
    return res.data
  }
}

