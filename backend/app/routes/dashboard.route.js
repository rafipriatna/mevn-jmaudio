const express = require("express");
const passport = require("passport");

const router = express.Router();

const transaksi = require("../controllers/transaksi.controller");
const pelanggan = require("../controllers/pelanggan.controller");
const barang = require("../controllers/admin/barang.controller");
const paket = require("../controllers/admin/paket.controller");

router.get(
  "/transaksiBulanIni",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  transaksi.transaksiBulanIni
);

router.get(
  "/totalPelanggan",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  pelanggan.totalPelanggan
);

router.get(
  "/totalBarang",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  barang.totalBarang
);

router.get(
  "/totalPaket",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  paket.totalPaket
);

module.exports = router;
