const moment = require("moment");
const model = require("../models/rekening.model");

exports.create = (req, res) => {
  let newRekening = new model(req.body);
  newRekening.save(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: `Rekening berhasil dibuat.`,
      data: data,
    });
  });
};

exports.getAll = (req, res) => {
  model.find(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: "Data rekening.",
      data: data,
    });
  });
};

exports.getById = (req, res) => {
  model.findById(req.params.id, function (err, data) {
    if (data == null)
      return res.status(404).json({
        message: "Data tidak ditemukan.",
      });
    res.status(200).json({
      message: "Data rekening.",
      data: data,
    });
  });
};

exports.update = (req, res) => {
  model.findByIdAndUpdate(
    req.params.id,
    req.body,
    {
      upsert: true,
      new: true,
    },
    function (err, data) {
      if (err) return res.status(500).json(err);
      res.status(200).json({
        message: `${data.nama_rekening} berhasil diperbarui!`,
        data: data,
      });
    }
  );
};

exports.delete = (req, res) => {
  model.findByIdAndDelete(req.params.id, function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: `${data.nama_rekening} berhasil dihapus!`,
    });
  });
};
