const multer = require("multer");
const fs = require("fs");
const bcrypt = require("bcryptjs");
const userModel = require("../../models/user.model");

// File Upload
const path = "public/user/";
const fileFilter = (req, file, callback) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    callback(null, true);
  } else {
    callback(null, false);
  }
};
const upload = multer({
  dest: path,
  limits: {
    fieldSize: 1024 * 1024 * 3, // 3MB
  },
  fileFilter: fileFilter,
}).single("foto_user");

exports.create = (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      res.status(500).json({
        message: "File upload error. Format file harus JPG atau PNG.",
      });
    }
    if (req.file) {
      req.body.foto_user = req.file.filename;
    }
    let newUser = new userModel(req.body);
    // Create the user
    newUser.save(function (err, data) {
      if (err) {
        if (req.file) {
          fs.unlinkSync(path + req.file.filename);
        }
        if (err.code == 11000)
          return res.status(403).json({
            message: "E-mail sudah dipakai.",
          });
        res.status(500).json(err);
      } else {
        res.status(200).json({
          message: `${data.nama_user} berhasil dibuat.`,
          data: data,
        });
      }
    });
  });
};

exports.getAll = (req, res) => {
  userModel.find(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: "Data users.",
      data: data,
    });
  });
};

exports.getById = (req, res) => {
  userModel.findById(req.params.id, function (err, data) {
    if (err) return res.status(500).json(err);
    if (data == null)
      return res.status(404).json({
        message: "User tidak ditemukan.",
      });
    res.status(200).json({
      message: "Data user.",
      data: data,
    });
  });
};

exports.update = (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      res.status(500).json({
        message: "File upload error. Format file harus JPG atau PNG.",
      });
    }
    if (req.file) {
      req.body.foto_user = req.file.filename;
    }

    if (!req.params.id) {
      req.params.id = req.user._id;
    }

    userModel.findByIdAndUpdate(
      req.params.id,
      req.body,
      {
        runValidators: true,
      },
      function (err, currentUser) {
        if (err) {
          if (err.code === 11000) {
            if (req.file) {
              fs.unlinkSync(path + req.file.filename);
            }
            return res.status(403).json({
              message: "E-mail sudah dipakai.",
            });
          } else {
            return res.status(500).json(err);
          }
        }
        // Delete current foto_user
        if (req.file) {
          if (fs.existsSync(path + currentUser.foto_user)) {
            fs.unlinkSync(path + currentUser.foto_user);
          }
        }
        res.status(200).json({
          message: `${currentUser.nama_user} berhasil diperbarui!`,
          data: currentUser,
        });
      }
    );
  });
};

exports.delete = (req, res) => {
  userModel
    .findByIdAndDelete(req.params.id, function (err, docs) {
      if (err) return res.status(500).json(err);
      if (docs != null) {
        if (fs.existsSync(path + docs.foto_user)) {
          fs.unlinkSync(path + docs.foto_user);
        }
      }
      res.status(200).json({
        message: `${docs.nama_user} berhasil dihapus!`,
      });
    })
    .catch((error) => {
      res.status(500).json(error);
    });
};

exports.gantiPassword = (req, res) => {
  userModel.findById(req.user._id, async function (err, data) {
    if (err) return res.status(500).json(err);
    if (data == null)
      return res.status(404).json({
        message: "User tidak ditemukan.",
      });

    let cocokin = await comparePassword(req.body, data);
    if (cocokin) {
      const passwordBaru = req.body.newPass;
      userModel.findByIdAndUpdate(
        req.user._id,
        {
          password: passwordBaru,
        },
        {
          runValidators: true,
        },
        function (err, currentUser) {
          if (err) return res.status(500).json(err);
          res.status(200).json({
            message: `Berhasil mengganti password!`,
            data: currentUser,
          });
        }
      );
    } else {
      return res.status(403).json({
        message: "Password tidak cocok.",
      });
    }
  });
};

comparePassword = async (userData, dbData) => {
  return bcrypt
    .compare(userData.newPass, dbData.password_user)
    .then((isMatched) => {
      if (!isMatched) {
        return false;
      }
      return true;
    })
    .catch(() => {
      return false;
    });
};
