const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const pelangganModel = require("../models/pelanggan.model");

const secretKey = require("../config/mongo.config").secret;

exports.create = (req, res) => {
  if (
    req.body.nomor_telepon_pelanggan.length > 12 ||
    req.body.nomor_telepon_pelanggan.length < 11 ||
    isNaN(req.body.nomor_telepon_pelanggan)
  ) {
    res.status(403).json({ message: "Format nomor telepon tidak valid." });
  } else {
    let newPelanggan = new pelangganModel(req.body);
    newPelanggan.save(function (err, data) {
      if (err) return res.status(500).json(err);
      res.status(200).json({
        message: `${data.nama_pelanggan} berhasil dibuat.`,
        data: data,
      });
    });
  }
};

exports.getAll = (req, res) => {
  pelangganModel.find(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: "Data pelanggan.",
      data: data,
    });
  });
};

exports.getById = (req, res) => {
  pelangganModel.findById(req.params.id, function (err, data) {
    if (data == null)
      return res.status(404).json({
        message: "Data tidak ditemukan.",
      });

    res.status(200).json({
      message: "Data pelanggan.",
      data: data,
    });
  });
};

exports.update = (req, res) => {
  if (
    req.body.nomor_telepon_pelanggan !== undefined &&
    (req.body.nomor_telepon_pelanggan.length > 12 ||
      req.body.nomor_telepon_pelanggan.length < 11 ||
      isNaN(req.body.nomor_telepon_pelanggan))
  ) {
    res.status(403).json({ message: "Format nomor telepon tidak valid." });
  } else {
    pelangganModel.findByIdAndUpdate(
      req.params.id ? req.params.id : req.user._id,
      req.body,
      {
        runValidators: true,
      },
      function (err, data) {
        if (err) return res.status(500).json(err);
        res.status(200).json({
          message: `${data.nama_pelanggan} berhasil diperbarui!`,
          data: data,
        });
      }
    );
  }
};

exports.delete = (req, res) => {
  pelangganModel.findByIdAndDelete(req.params.id, function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: `${data.nama_pelanggan} berhasil dihapus!`,
    });
  });
};

/**
 * Login
 */
exports.login = (req, res) => {
  pelangganModel.findOne(
    {
      email_pelanggan: req.body.email_pelanggan,
    },
    async function (err, data) {
      if (data == null) {
        res.status(404).json({
          message: "Data tidak ditemukan.",
        });
      } else {
        // Compare Password
        let token = await comparePassword(req.body, data);
        if (!token) {
          res.status(401).json({
            message: "Surel atau password salah.",
          });
        } else {
          res.status(200).json({
            message: "Barhasil masuk.",
            token: `${token}`,
          });
        }
      }
    }
  );
};

// Total Pelanggan
exports.totalPelanggan = (req, res) => {
  pelangganModel.find(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: "Pelanggan.",
      data: data.length,
    });
  });
};

exports.gantiPassword = (req, res) => {
  pelangganModel.findById(req.user._id, async function (err, data) {
    if (err) return res.status(500).json(err);
    if (data == null)
      return res.status(404).json({
        message: "User tidak ditemukan.",
      });

    let cocokin = await comparePassword(req.body, data);
    if (cocokin) {
      const passwordBaru = req.body.newPass;
      pelangganModel.findByIdAndUpdate(
        req.user._id,
        {
          password_pelanggan: passwordBaru,
        },
        {
          runValidators: true,
        },
        function (err, currentPelanggan) {
          if (err) return res.status(500).json(err);
          res.status(200).json({
            message: `Berhasil mengganti password!`,
            data: currentPelanggan,
          });
        }
      );
    } else {
      return res.status(403).json({
        message: "Password tidak cocok.",
      });
    }
  });
};

comparePassword = async (userData, dbData) => {
  return bcrypt
    .compare(userData.password_pelanggan, dbData.password_pelanggan)
    .then((isMatched) => {
      if (!isMatched) {
        return false;
      } else {
        // Generate Token
        const payload = {
          _id: dbData._id,
        };

        return jwt.sign(payload, secretKey, {
          expiresIn: 604800, // 7 Days
        });
      }
    })
    .catch((error) => {
      return false;
    });
};
