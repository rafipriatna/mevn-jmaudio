import axios from "../../axios";

const state = {};
const getters = {};
const actions = {
  async get_all_pelanggan() {
    let res = await axios.get("/pegawai/pelanggan");
    return res.data.data;
  },
  async create_new_pelanggan(context, data) {
    let res = await axios.post("/pegawai/pelanggan", data);
    return res.data.message;
  },
  async get_pelanggan(context, id) {
    let res = await axios.get(`/pegawai/pelanggan/${id}`);
    return res.data.data;
  },
  async update_pelanggan(context, data) {
    const id = data.id ? data.id : data.get("id");
    let res = await axios.patch(`/pegawai/pelanggan/${id}`, data);
    return res.data.message;
  },
  async delete_pelanggan(context, id) {
    let res = await axios.delete(`/pegawai/pelanggan/${id}`);
    return res.data.message;
  },
};
const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
