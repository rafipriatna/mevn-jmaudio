const passportJWT = require("passport-jwt");

const strategyJWT = passportJWT.Strategy;
const extractJWT = passportJWT.ExtractJwt;

const userModel = require("../models/user.model");
const pelangganModel = require("../models/pelanggan.model");
const transaksiModel = require("../models/transaksi.model");
const secretKey = require("../config/mongo.config").secret;

const options = {};
options.jwtFromRequest = extractJWT.fromAuthHeaderAsBearerToken();
options.secretOrKey = secretKey;

module.exports = (passport) => {
  // All privileges
  passport.use(
    "needAuthentication",
    new strategyJWT(options, (payloadJWT, done) => {
      userModel
        .findById(payloadJWT._id)
        .then((user) => {
          if (!user) return done(null, false);
          return done(null, user);
        })
        .catch((err) => {
          return done(null, false, {
            error: err,
          });
        });
    })
  );

  // Admin Privileges
  passport.use(
    "adminPriveleges",
    new strategyJWT(options, (payloadJWT, done) => {
      userModel
        .findById(payloadJWT._id)
        .then((user) => {
          if (!user) return done(null, false);
          if (user.level_user != "Admin") return done(null, false);

          return done(null, user);
        })
        .catch((err) => {
          return done(null, false, {
            error: err,
          });
        });
    })
  );

  // Pelanggan Privileges
  passport.use(
    "pelangganAuth",
    new strategyJWT(options, (payloadJWT, done) => {
      pelangganModel
        .findById(payloadJWT._id)
        .then((pelanggan) => {
          if (!pelanggan) return done(null, false);
          return done(null, pelanggan);
        })
        .catch((err) => {
          return done(null, false, {
            error: err,
          });
        });
    })
  );

  // Logged Pelanggan Only Privileges
  passport.use(
    "loggedPelangganOnlyTransaction",
    new strategyJWT(options, (payloadJWT, done) => {
      pelangganModel
        .findById(payloadJWT._id)
        .then((pelanggan) => {
          if (!pelanggan) return done(null, false);
          transaksiModel
            .find({
              pelanggan_id: payloadJWT._id,
            })
            .populate("paket_transaksi", ["nama_paket", "harga_paket"])
            .populate("pembayaran_transaksi.rekening_tujuan_id")
            .then((transaksi) => {
              if (!transaksi) return done(null, false);
              return done(null, transaksi);
            })
            .catch((err) => {
              return done(null, false, {
                error: err,
              });
            });
        })
        .catch((err) => {
          return done(null, false, {
            error: err,
          });
        });
    })
  );
};
