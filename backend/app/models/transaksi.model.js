const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const TransaksiSchema = new Schema(
  {
    no_transaksi: {
      type: String,
      unique: true,
      required: true,
    },
    pelanggan_id: {
      type: Schema.ObjectId,
      ref: "pelanggan",
      required: true,
    },
    paket_transaksi: {
      nama_paket: {
        type: String,
        required: true,
      },
      watt_paket: {
        type: Number,
        required: true,
      },
      keterangan_paket: {
        type: String,
        required: true,
      },
      harga_paket: {
        type: Number,
        required: true,
      },
      foto_paket: {
        type: String,
        required: true,
      },
    },
    catatan_transaksi: {
      type: String,
      default: "Tanpa keterangan.",
    },
    dp_transaksi: {
      type: String,
      required: true,
    },
    total_harga_transaksi: {
      type: String,
      required: true,
    },
    status_transaksi: {
      type: String,
      enum: [
        "Belum Bayar",
        "Menunggu Konfirmasi Pembayaran",
        "Sudah Bayar DP",
        "Dibatalkan",
        "Selesai",
      ],
      default: "Belum Bayar",
      required: true,
    },
    tanggal_transaksi: {
      dari: {
        type: Date,
        required: true,
      },
      sampai: {
        type: Date,
        required: true,
      },
    },
    pembayaran_transaksi: {
      nama_pengirim: {
        type: String,
      },
      bank_pengirim: {
        type: String,
      },
      rekening_pengirim: {
        type: String,
      },
      rekening_tujuan_id: {
        type: Schema.ObjectId,
        ref: "rekening",
      },
      bukti_pembayaran: {
        type: String,
      },
      pembayaran_diperbarui: {
        type: Date,
      },
    },
  },
  { collection: "transaksi", timestamps: true }
);

module.exports = mongoose.model("transaksi", TransaksiSchema);
