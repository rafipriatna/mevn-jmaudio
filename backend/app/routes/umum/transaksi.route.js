const express = require("express");
const passport = require("passport");

const router = express.Router();

const controller = require("../../controllers/transaksi.controller");

// Create new transaction
router.post(
  "/",
  passport.authenticate("pelangganAuth", {
    session: false,
  }),
  controller.create
);

// Get only logged pelanggan transaction
router.get(
  "/",
  passport.authenticate("loggedPelangganOnlyTransaction", {
    session: false,
  }),
  (req, res) => {
    return res.status(200).json({
      message: "Transaksi Anda.",
      data: req.user,
    });
  }
);

router.get(
  "/:id",
  passport.authenticate("pelangganAuth", {
    session: false,
  }),
  controller.getById
);

router.patch(
  "/:id",
  passport.authenticate("pelangganAuth", {
    session: false,
  }),
  controller.uploadBuktiPembayaran
);

router.delete(
  "/:id",
  passport.authenticate("pelangganAuth", {
    session: false,
  }),
  controller.batalkanPesanan
);

module.exports = router;
