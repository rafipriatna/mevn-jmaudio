const multer = require("multer");
const fs = require("fs");
const paketModel = require("../../models/paket.model");

// File Upload
const path = "public/paket/";
const fileFilter = (req, file, callback) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    callback(null, true);
  } else {
    callback(null, false);
  }
};
const upload = multer({
  dest: path,
  limits: {
    fieldSize: 1024 * 1024 * 3, // 3MB
  },
  fileFilter: fileFilter,
}).single("foto_paket");

exports.create = (req, res) => {
  upload(req, res, (err) => {
    if (err || !req.file)
      return res.status(500).json({
        message: "File upload error. Format file harus JPG atau PNG.",
      });
    req.body.foto_paket = req.file.filename;
    req.body.barang_paket = JSON.parse(req.body.barang_paket);
    let newPaket = new paketModel(req.body);
    newPaket.save(function (err, data) {
      if (err) {
        fs.unlinkSync(req.file.path);
        return res.status(500).json(err);
      }
      res.status(200).json({
        message: `${data.nama_paket} berhasil dibuat.`,
        data: data,
      });
    });
  });
};

exports.getAll = (req, res) => {
  paketModel
    .find(function (err, data) {
      if (err) return res.status(500).json(err);
      res.status(200).json({
        message: "Data paket.",
        data: data,
      });
    })
    .populate("barang_paket.data_barang");
};

exports.getById = (req, res) => {
  paketModel
    .findById(req.params.id, function (err, data) {
      if (data == null)
        return res.status(404).json({
          message: "Data tidak ditemukan.",
        });

      res.status(200).json({
        message: "Data paket.",
        data: data,
      });
    })
    .populate("barang_paket.data_barang");
};

exports.update = (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      return res.status(500).json({
        message: "File upload error. Format file harus JPG atau PNG.",
      });
    }
    if (req.file) {
      req.body.foto_paket = req.file.filename;
    }
    req.body.barang_paket = JSON.parse(req.body.barang_paket);
    paketModel
      .findByIdAndUpdate(
        req.params.id,
        req.body,
        {
          runValidators: true,
        },
        function (err, currentPaket) {
          if (err) {
            if (req.file) {
              fs.unlinkSync(path + req.file.filename);
            }
            return res.status(500).json(err);
          }
          // Delete current photo
          if (req.file) {
            if (fs.existsSync(path + currentPaket.foto_paket)) {
              fs.unlinkSync(path + currentPaket.foto_paket);
            }
          }
          res.status(200).json({
            message: `${currentPaket.nama_paket} berhasil diperbarui!`,
            data: currentPaket,
          });
        }
      )
      .populate("barang_paket.data_barang");
  });
};

exports.delete = (req, res) => {
  paketModel.findByIdAndDelete(req.params.id, function (err, data) {
    if (err) return res.status(500).json(err);
    if (data != null) {
      if (fs.existsSync(path + data.foto_paket)) {
        fs.unlinkSync(path + data.foto_paket);
      }
    }
    res.status(200).json({
      message: `${data.nama_paket} berhasil dihapus!`,
    });
  });
};

exports.totalPaket = (req, res) => {
  paketModel.find(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: "Paket.",
      data: data.length,
    });
  });
};
