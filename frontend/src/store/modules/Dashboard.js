import axios from "../../axios";

const state = {};
const getters = {};
const actions = {
  async get_transaksi_bulan_ini() {
    let res = await axios.get("/pegawai/dashboard/transaksiBulanIni");
    return res.data.data;
  },
  async get_total_pelanggan() {
    let res = await axios.get("/pegawai/dashboard/totalPelanggan");
    return res.data.data;
  },
  async get_total_barang() {
    let res = await axios.get("/pegawai/dashboard/totalBarang");
    return res.data.data;
  },
  async get_total_paket() {
    let res = await axios.get("/pegawai/dashboard/totalPaket");
    return res.data.data;
  },
};
const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
