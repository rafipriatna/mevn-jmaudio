const express = require("express");
const passport = require("passport");

const router = express.Router();

const controller = require("../../controllers/rekening.controller");

router.get("/", controller.getAll);

module.exports = router;
