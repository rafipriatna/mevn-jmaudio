import axios from "../../axios";

const state = {};
const getters = {};
const actions = {
  async get_all_jenis_barang() {
    let res = await axios.get("/admin/jenis_barang");
    return res.data.data;
  },
  async create_new_jenis_barang(context, data) {
    let res = await axios.post("/admin/jenis_barang", data);
    return res.data.message;
  },
  async get_jenis_barang(context, id) {
    let res = await axios.get(`/admin/jenis_barang/${id}`);
    return res.data.data;
  },
  async update_jenis_barang(context, data) {
    let id = data.id;
    let res = await axios.patch(`/admin/jenis_barang/${id}`, data);
    return res.data.message;
  },
  async delete_jenis_barang(context, id) {
    let res = await axios.delete(`/admin/jenis_barang/${id}`);
    return res.data.message;
  },
};
const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
