const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const RekeningSchema = new Schema(
  {
    nama_rekening: {
      type: String,
      required: true,
    },
    no_rekening: {
      type: String,
      required: true,
    },
    bank: {
      type: String,
      required: true,
    },
  },
  { collection: "rekening" }
);

module.exports = mongoose.model("rekening", RekeningSchema);
