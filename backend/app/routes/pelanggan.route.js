const express = require("express");
const passport = require("passport");

const router = express.Router();

const model = require("../controllers/pelanggan.controller");

router.post(
  "/",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  model.create
);

router.get(
  "/",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  model.getAll
);

router.get(
  "/:id",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  model.getById
);

router.patch(
  "/:id",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  model.update
);

router.delete(
  "/:id",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  model.delete
);

module.exports = router;
