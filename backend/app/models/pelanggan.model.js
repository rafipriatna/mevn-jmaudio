const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const Schema = mongoose.Schema;

const validateEmail = function (email) {
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  return re.test(email);
};

const PelangganSchema = new Schema(
  {
    email_pelanggan: {
      type: String,
      required: true,
      unique: true,
      lowercase: true,
      validate: [validateEmail, "Surel tidak valid!"],
      match: [
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        "Surel tidak valid!",
      ],
    },
    nama_pelanggan: {
      type: String,
      required: true,
    },
    alamat_pelanggan: {
      type: String,
      required: true,
    },
    kecamatan_pelanggan: {
      type: String,
      required: true,
    },
    kelurahan_pelanggan: {
      type: String,
      required: true,
    },
    password_pelanggan: {
      type: String,
      required: true,
    },
    nomor_telepon_pelanggan: {
      type: String,
      required: true,
      unique: true,
    },
  },
  { collection: "pelanggan" }
);

PelangganSchema.pre("save", function (next) {
  //Mongoose middleware, performs password hashing before saving data into database
  let pelanggan = this;
  bcrypt.genSalt(10, (error, salt) => {
    bcrypt.hash(pelanggan.password_pelanggan, salt, (error, hash) => {
      pelanggan.password_pelanggan = hash;
      next();
    });
  });
});

PelangganSchema.pre("findOneAndUpdate", function (next) {
  let pelanggan = this._update;
  let isModified = pelanggan.password_pelanggan;
  if (isModified) {
    //Check if the password is modifed, if so then only perform hasing
    bcrypt.genSalt(10, (error, salt) => {
      bcrypt.hash(pelanggan.password_pelanggan, salt, (error, hash) => {
        pelanggan.password_pelanggan = hash;
        next();
      });
    });
  } else {
    next();
  }
});

module.exports = mongoose.model("pelanggan", PelangganSchema);
