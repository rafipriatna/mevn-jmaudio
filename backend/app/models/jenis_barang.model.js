const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const JenisBarangSchema = new Schema(
  {
    nama_jenis_barang: {
      type: String,
      required: true,
    },
  },
  { collection: "jenis_barang" }
);

module.exports = Barang = mongoose.model("jenis_barang", JenisBarangSchema);
