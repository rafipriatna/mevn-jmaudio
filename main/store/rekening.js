export const actions = {
    async getAllRekening() {
      let res = await this.$axios.$get(`${this.$axios.defaults.baseURL}/rekening`)
      return res.data
    },
  }
