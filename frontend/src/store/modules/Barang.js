import axios from "../../axios";

const state = {};
const getters = {};
const actions = {
  async get_all_barang() {
    let res = await axios.get("/admin/barang");
    return res.data.data;
  },
  async create_new_barang(context, data) {
    let res = await axios.post("/admin/barang", data);
    return res.data.message;
  },
  async get_barang(context, id) {
    let res = await axios.get(`/admin/barang/${id}`);
    return res.data.data;
  },
  async update_barang(context, data) {
    const id = data.id ? data.id : data.get("id");
    let res = await axios.patch(`/admin/barang/${id}`, data);
    return res.data.message;
  },
  async delete_barang(context, id) {
    let res = await axios.delete(`/admin/barang/${id}`);
    return res.data.message;
  },
};
const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
