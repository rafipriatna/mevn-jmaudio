const multer = require("multer");
const fs = require("fs");
const barangModel = require("../../models/barang.model");

// File Upload
const path = "public/barang/";
const fileFilter = (req, file, callback) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    callback(null, true);
  } else {
    callback(null, false);
  }
};
const upload = multer({
  dest: path,
  limits: {
    fieldSize: 1024 * 1024 * 3, // 3MB
  },
  fileFilter: fileFilter,
}).single("foto_barang");

exports.create = (req, res) => {
  upload(req, res, (err) => {
    if (err || !req.file)
      return res.status(500).json({
        message: "File upload error. Format file harus JPG atau PNG.",
      });
    req.body.foto_barang = req.file.filename;
    let newBarang = new barangModel(req.body);
    newBarang.save(function (err, data) {
      if (err) {
        fs.unlinkSync(req.file.path);
        if (err.code == 11000)
          return res.status(403).json({
            message: "Nomor seri sudah dipakai.",
          });
        return res.status(500).json(err);
      } else {
        res.status(200).json({
          message: `${data.nama_barang} berhasil dibuat!`,
          data: data,
        });
      }
    });
  });
};

exports.getAll = (req, res) => {
  barangModel
    .find(function (err, data) {
      if (err) return res.status(500).json(err);
      res.status(200).json({
        message: "Data barang.",
        data: data,
      });
    })
    .populate("jenis_barang_id")
    .populate("merek_barang_id");
};

exports.getById = (req, res) => {
  barangModel
    .findById(req.params.id, function (err, data) {
      if (data == null)
        return res.status(404).json({
          message: "Data tidak ditemukan.",
        });

      res.status(200).json({
        message: "Data barang.",
        data: data,
      });
    })
    .populate("jenis_barang_id")
    .populate("merek_barang_id");
};

exports.update = (req, res) => {
  upload(req, res, (err) => {
    if (err) {
      res.status(500).json({
        message: "File upload error. Format file harus JPG atau PNG.",
      });
    }
    if (req.file) {
      req.body.foto_barang = req.file.filename;
    }
    barangModel
      .findByIdAndUpdate(
        req.params.id,
        req.body,
        {
          runValidators: true,
        },
        function (err, currentBarang) {
          if (err) {
            if (err.code === 11000) {
              if (req.file) {
                fs.unlinkSync(path + req.file.filename);
              }
              return res.status(403).json({
                message: "Nomor seri sudah dipakai.",
              });
            } else {
              return res.status(500).json(err);
            }
          }
          // Delete current photo
          if (req.file) {
            if (fs.existsSync(path + currentBarang.foto)) {
              fs.unlinkSync(path + currentBarang.foto);
            }
          }
          res.status(200).json({
            message: `${currentBarang.nama_barang} berhasil diperbarui!`,
            data: currentBarang,
          });
        }
      )
      .populate("jenis_barang_id")
      .populate("merek_barang_id");
  });
};

exports.delete = (req, res) => {
  barangModel.findByIdAndDelete(req.params.id, function (err, data) {
    if (err) return res.status(500).json(err);
    if (data != null) {
      if (fs.existsSync(path + data.foto_barang)) {
        fs.unlinkSync(path + data.foto_barang);
      }
    }
    res.status(200).json({
      message: `${data.nama_barang} berhasil dihapus!`,
    });
  });
};

exports.totalBarang = (req, res) => {
  barangModel.find(function (err, data) {
    if (err) return res.status(500).json(err);
    res.status(200).json({
      message: "Barang.",
      data: data.length,
    });
  });
};
