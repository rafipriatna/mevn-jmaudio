const moment = require("moment");
const model = require("../models/transaksi.model");

exports.bulanIni = (req, res) => {
  mulaiBulan = moment().startOf("month");
  akhirBulan = moment().endOf("month");
  model
    .find(
      {
        "tanggal_transaksi.dari": {
          $gte: mulaiBulan,
          $lte: akhirBulan,
        },
        $or: [
          {
            status_transaksi: "Sudah Bayar DP",
          },
          {
            status_transaksi: "Selesai",
          },
        ],
      },
      function (err, data) {
        if (err) return res.status(500).json(err);
        res.status(200).json({
          message: `Transaksi bulan ${moment().subtract(1, "month").startOf("month").format('MMMM')}`,
          data: data,
        });
      }
    )
    .populate("pelanggan_id")
    .populate("paket_transaksi", ["nama_paket", "harga_paket"])
    .populate("pembayaran_transaksi.rekening_tujuan_id");
};

exports.pilihTanggal = (req, res) => {
  console.log(req.body.dari);
  awalTanggal = req.body.dari
  akhirTanggal = req.body.sampai;
  model
    .find(
      {
        "tanggal_transaksi.dari": {
          $gte: awalTanggal,
          $lte: akhirTanggal,
        },
        $or: [
          {
            status_transaksi: "Sudah Bayar DP",
          },
          {
            status_transaksi: "Selesai",
          },
        ],
      },
      function (err, data) {
        if (err) return res.status(500).json(err);
        res.status(200).json({
          message: `Transaksi dari tanggal ${moment(awalTanggal).format(
            "DD/MM/YYYY"
          )} sampai ${moment(akhirTanggal).format("DD/MM/YYYY")}`,
          data: data,
        });
      }
    )
    .populate("pelanggan_id")
    .populate("paket_transaksi", ["nama_paket", "harga_paket"])
    .populate("pembayaran_transaksi.rekening_tujuan_id");
};
