export const actions = {
    async getAllGaleri() {
      let res = await this.$axios.$get(`${this.$axios.defaults.baseURL}/galeri`)
      return res.data
    },
}