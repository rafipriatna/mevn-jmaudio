// Component
const IndexView = () =>
  import(
    /* webpackChunkName: "group-barang" */ "@/views/admin/barang/Barang.vue"
  );
const TambahView = () =>
  import(
    /* webpackChunkName: "group-barang" */ "@/views/admin/barang/Tambah.vue"
  );
const UbahView = () =>
  import(
    /* webpackChunkName: "group-barang" */ "@/views/admin/barang/Ubah.vue"
  );

// Routes
export default [
  {
    path: "/barang",
    name: "Barang",
    component: IndexView,
    meta: {
      title: `Data Barang — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
  {
    path: "/barang/tambah",
    name: "TambahBarang",
    component: TambahView,
    meta: {
      title: `Tambah Data Barang — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
  {
    path: "/barang/:id/",
    name: "UbahBarang",
    component: UbahView,
    meta: {
      title: `Ubah Data Barang — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
];
