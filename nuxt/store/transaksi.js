import axios from 'axios'

export const actions = {
  async create_transaksi(context, data) {
    if (process.browser) {
      const token = localStorage.getItem('auth._token.local')
      let res = await axios.post(
        `${this.$axios.defaults.baseURL}/transaksi`,
        data,
        {
          headers: {
            Authorization: token,
          },
        }
      )
      return res.data.data
    }
  },
  async get_all_transaksi() {
    if (process.browser) {
      const token = localStorage.getItem('auth._token.local')
      let res = await axios.get(`${this.$axios.defaults.baseURL}/transaksi`, {
        headers: {
          Authorization: token,
        },
      })
      return res.data.data
    }
  },
  async get_transaksi(context, id) {
    if (process.browser) {
      const token = localStorage.getItem('auth._token.local')
      let res = await axios.get(
        `${this.$axios.defaults.baseURL}/transaksi/${id}`,
        {
          headers: {
            Authorization: token,
          },
        }
      )
      return res.data.data
    }
  },
  async update_transaksi(context, data) {
    if (process.browser) {
      const id = data.get('id')
      const token = localStorage.getItem('auth._token.local')
      let res = await axios.patch(
        `${this.$axios.defaults.baseURL}/transaksi/${id}`,
        data,
        {
          headers: {
            Authorization: token,
          },
        }
      )
      return res.data.message
    }
  },
  async batalkan_pesanan(context, id) {
    if (process.browser) {
      const token = localStorage.getItem('auth._token.local')
      let res = await axios.delete(
        `${this.$axios.defaults.baseURL}/transaksi/${id}`,
        {
          headers: {
            Authorization: token,
          },
        }
      )
      return res.data.message
    }
  },
}
