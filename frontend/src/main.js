import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

// Templates
// General CSS Files
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";

// Stisla
import "./assets/css/style.css";
import "./assets/css/components.css";

// Scripts
import "jquery/dist/jquery";
import "jquery-ui-dist/jquery-ui";
import "popper.js/dist/popper";
import "tooltip.js/dist/tooltip";
import "bootstrap/dist/js/bootstrap";
import "nicescroll/jquery.nicescroll";
import "moment/moment";
import "./assets/js/scripts";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
