const express = require("express");
const passport = require("passport");

const router = express.Router();
const user = require("../../controllers/admin/user.controller");

router.post(
  "/",
  passport.authenticate("adminPriveleges", {
    session: false,
  }),
  user.create
);

router.get(
  "/",
  passport.authenticate("adminPriveleges", {
    session: false,
  }),
  user.getAll
);

router.get(
  "/:id",
  passport.authenticate("adminPriveleges", {
    session: false,
  }),
  user.getById
);

router.patch(
  "/:id",
  passport.authenticate("adminPriveleges", {
    session: false,
  }),
  user.update
);

router.delete(
  "/:id",
  passport.authenticate("adminPriveleges", {
    session: false,
  }),
  user.delete
);

module.exports = router;
