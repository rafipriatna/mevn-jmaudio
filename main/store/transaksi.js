export const actions = {
    async getAllTransaksi() {
      let res = await this.$axios.$get(`${this.$axios.defaults.baseURL}/transaksi`)
      return res.data
    },
    async getTransaksi(context, id) {
      let res = await this.$axios.$get(`${this.$axios.defaults.baseURL}/transaksi/${id}`)
      return res.data
    },
    async updateTransaksi(context, data) {
      const id = data.get('id')
      let res = await this.$axios.$patch(`${this.$axios.defaults.baseURL}/transaksi/${id}`, data)
      return res.message
    },
    async batalkanTransaksi(context, id) {
      let res = await this.$axios.$delete(`${this.$axios.defaults.baseURL}/transaksi/${id}`)
      return res.message
    },
  }
