const userModel = require("../models/user.model");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const secretKey = require("../config/mongo.config").secret;

exports.register = (req, res) => {
  // Check if user already exists.
  userModel
    .find()
    .then((user) => {
      if (user.length > 0) {
        res.status(400).json({
          message: "User already exists. Register disabled.",
        });
      } else {
        // Register the user.
        let newUser = new userModel(req.body);

        newUser.save().then((user) => {
          return res
            .status(200)
            .json({
              message: "User is now registered.",
            })
            .catch((error) => {
              res.status(500).json({
                error: error,
              });
            });
        });
      }
    })
    .catch((error) => {
      res.status(500).json(error);
    });
};

exports.login = (req, res) => {
  userModel
    .findOne({
      email_user: req.body.email_user,
    })
    .then(async (user) => {
      // Check users
      if (!user) {
        res.status(404).json({
          message: "Pengguna tidak ditemukan.",
        });
      } else {
        // Compare Password
        let token = await cekLogin(req.body, user);
        if (!token) {
          res.status(401).json({
            message: "Email atau password salah.",
          });
        } else {
          // Update last access
          userModel
            .findByIdAndUpdate(user._id, {
              last_access_user: Date.now(),
            })
            .then((result) => {
              res.status(200).json({
                message: "Barhasil masuk.",
                token: `Bearer ${token}`,
              });
            })
            .catch((error) => {
              res.status(500).json(error);
            });
        }
      }
    })
    .catch((error) => {
      res.status(500).json(error);
    });
};

cekLogin = async (userData, dbData) => {
  return bcrypt
    .compare(userData.password_user, dbData.password_user)
    .then((isMatched) => {
      if (!isMatched) {
        return false;
      } else {
        // Generate Token
        const payload = {
          _id: dbData._id,
          level_user: dbData.level_user,
          last_access_user: dbData.last_access_user,
        };

        return jwt.sign(payload, secretKey, {
          expiresIn: 604800, // 7 Days
        });
      }
    })
    .catch(() => {
      return false;
    });
};
