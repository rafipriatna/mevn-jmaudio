// Component
const indexView = () =>
  import(
    /* webpackChunkName: "group-pelanggan" */ "@/views/pelanggan/Pelanggan.vue"
  );
const createView = () =>
  import(/* webpackChunkName: "group-pelanggan" */ "@/views/pelanggan/Tambah.vue");
const editView = () =>
  import(
    /* webpackChunkName: "group-pelanggan" */ "@/views/pelanggan/Ubah.vue"
  );

// Routes
export default [
  {
    path: "/pelanggan",
    name: "Pelanggan",
    component: indexView,
    meta: {
      title: `Data Pelanggan — ${process.env.VUE_APP_SITE_TITLE}`,
    },
  },
  {
    path: "/pelanggan/tambah",
    name: "TambahPelanggan",
    component: createView,
    meta: {
      title: `Tambah Pelanggan — ${process.env.VUE_APP_SITE_TITLE}`,
    },
  },
    {
      path: "/pelanggan/:id",
      name: "UbahPelanggan",
      component: editView,
      meta: {
        title: `Ubah Pelanggan — ${process.env.VUE_APP_SITE_TITLE}`,
        isAdmin: true,
      },
    },
];
