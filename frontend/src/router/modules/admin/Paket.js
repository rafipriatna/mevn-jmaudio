// Component
const IndexView = () =>
  import(/* webpackChunkName: "group-paket" */ "@/views/admin/paket/Paket.vue");
const TambahView = () =>
  import(
    /* webpackChunkName: "group-paket" */ "@/views/admin/paket/Tambah.vue"
  );
const UbahView = () =>
  import(
    /* webpackChunkName: "group-paket" */ "@/views/admin/paket/Ubah.vue"
  );

// Routes
export default [
  {
    path: "/paket",
    name: "Paket",
    component: IndexView,
    meta: {
      title: `Data Paket — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
  {
    path: "/paket/tambah",
    name: "TambahPaket",
    component: TambahView,
    meta: {
      title: `Tambah Data Paket — ${process.env.VUE_APP_SITE_TITLE}`,
      isAdmin: true,
    },
  },
    {
      path: "/paket/:id/",
      name: "UbahPaket",
      component: UbahView,
      meta: {
        title: `Ubah Data Paket — ${process.env.VUE_APP_SITE_TITLE}`,
        isAdmin: true,
      },
    },
];
