const express = require("express");
const passport = require("passport");

const router = express.Router();

const model = require("../../controllers/admin/jenis_barang.controller");

router.post(
  "/",
  passport.authenticate("adminPriveleges", {
    session: false,
  }),
  model.create
);

router.get(
  "/",
  passport.authenticate("adminPriveleges", {
    session: false,
  }),
  model.getAll
);

router.get(
  "/:id",
  passport.authenticate("adminPriveleges", {
    session: false,
  }),
  model.getById
);

router.patch(
  "/:id",
  passport.authenticate("adminPriveleges", {
    session: false,
  }),
  model.update
);

router.delete(
  "/:id",
  passport.authenticate("adminPriveleges", {
    session: false,
  }),
  model.delete
);

module.exports = router;
