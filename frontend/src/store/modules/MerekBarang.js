import axios from "../../axios";

const state = {};
const getters = {};
const actions = {
  async get_all_merek_barang() {
    let res = await axios.get("/admin/merek_barang");
    return res.data.data;
  },
  async create_new_merek_barang(context, data) {
    let res = await axios.post("/admin/merek_barang", data);
    return res.data.message;
  },
  async get_merek_barang(context, id) {
    let res = await axios.get(`/admin/merek_barang/${id}`);
    return res.data.data;
  },
  async update_merek_barang(context, data) {
    let id = data.id;
    let res = await axios.patch(`/admin/merek_barang/${id}`, data);
    return res.data.message;
  },
  async delete_merek_barang(context, id) {
    let res = await axios.delete(`/admin/merek_barang/${id}`);
    return res.data.message;
  },
};
const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
