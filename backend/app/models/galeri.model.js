const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const GaleriSchema = new Schema(
  {
    judul_galeri: {
      type: String,
      required: true,
    },
    keterangan_galeri: {
      type: String,
      required: true,
    },
    foto_galeri: [
      {
        file: {
          type: String,
          required: true,
        },
      },
    ],
  },
  { collection: "galeri" }
);

module.exports = Galeri = mongoose.model("galeri", GaleriSchema);
