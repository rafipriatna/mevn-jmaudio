import axios from "../../axios";

const state = {};
const getters = {};
const actions = {
  async get_all_paket() {
    let res = await axios.get("/admin/paket");
    return res.data.data;
  },
  async create_new_paket(context, data) {
    let res = await axios.post("/admin/paket", data);
    return res.data.message;
  },
  async get_paket(context, id) {
    let res = await axios.get(`/admin/paket/${id}`);
    return res.data.data;
  },
  async update_paket(context, data) {
    const id = data.id ? data.id : data.get("id");
    let res = await axios.patch(`/admin/paket/${id}`, data);
    return res.data.message;
  },
  async delete_paket(context, id) {
    let res = await axios.delete(`/admin/paket/${id}`);
    return res.data.message;
  },
};
const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
