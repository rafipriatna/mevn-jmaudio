const express = require("express");
const passport = require("passport");

const router = express.Router();

const controller = require("../controllers/transaksi.controller");

router.get(
  "/",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  controller.getAll
);

router.get(
  "/:id",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  controller.getById
);

router.patch(
  "/:id",
  passport.authenticate("needAuthentication", {
    session: false,
  }),
  controller.updateStatus
);

module.exports = router;
