import axios from "../../axios";

const state = {};
const getters = {};
const actions = {
  async get_all_transaksi() {
    let res = await axios.get("/pegawai/transaksi");
    return res.data.data;
  },
  async create_new_transaksi(context, data) {
    let res = await axios.post("/pegawai/transaksi", data);
    return res.data.message;
  },
  async get_transaksi(context, id) {
    let res = await axios.get(`/pegawai/transaksi/${id}`);
    return res.data.data;
  },
  async update_transaksi(context, data) {
    const id = data.id;
    let res = await axios.patch(`/pegawai/transaksi/${id}`, data);
    return res.data.message;
  },
  async delete_transaksi(context, id) {
    let res = await axios.delete(`/pegawai/transaksi/${id}`);
    return res.data.message;
  },
};
const mutations = {};

export default {
  state,
  actions,
  mutations,
  getters,
};
