const express = require("express");
const passport = require("passport");
const router = express.Router();

const controller = require("../controllers/galeri.controller");

router.get(
    "/",
    passport.authenticate("needAuthentication", {
        session: false,
    }),
    controller.getAll
);

router.post(
    "/",
    passport.authenticate("needAuthentication", {
        session: false,
    }),
    controller.create
);

router.get(
    "/:id",
    passport.authenticate("needAuthentication", {
        session: false,
    }),
    controller.getById
);

router.patch(
    "/data/:id",
    passport.authenticate("needAuthentication", {
        session: false,
    }),
    controller.update
);

router.patch(
    "/foto/:id",
    passport.authenticate("needAuthentication", {
        session: false,
    }),
    controller.tambahFoto
);

router.patch(
    "/hapus/:id",
    passport.authenticate("needAuthentication", {
        session: false,
    }),
    controller.hapusFoto
);

router.delete(
    "/:id",
    passport.authenticate("adminPriveleges", {
        session: false,
    }),
    controller.hapusGaleri
);

module.exports = router;
